# MetaRouter Tools (Deprecated)

Public repository to host MetaRouter tools.

# Command Line Tools

Tools have migrated over to [metarouter/tools](https://gitlab.com/metarouter/tools)

## Migration

### Homebrew

```bash
brew untap --force metarouter/tools
brew tap --force-auto-update metarouter/tools https://gitlab.com/metarouter/tools.git && \
brew install metarouter/tools/cli 
```
